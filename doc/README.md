## Usage function

Usage function display Help. This functions reads the file run.sh:
lines which begins with 1 tab and ends by ")" are considred as $commands. 
Under this line, the followings lines will display helps associated to the commands:  
* lines with #o# display the options associated to the commands.  
* lines with #h# contains a brief description of the commands
* lines containing #### are interpreted like a title for a category of commands. The twos last characters represents 
the color in which help will be displayed for these commands

if you want to add or update the case function in run.sh sh, please respect this scheme:

```shell
case $1 in

    the-case)
        #o# [options availables for 'the-case' command]
        #h# a brief description
        the commands here
        another part of the commands
        ;;
    
```         

Please keep the case of run.sh ordered by category of command.
