#!/bin/bash

source .env
source ./doc/usage.sh

set -e # exit on error
case $1 in
#### DOCKER COMMANDS 32
  up)
    #h# Create and start the container
    docker-compose -p "${projectName}" -f ./docker/docker-compose.yml up -d ;;
  build)
    #h# Build or rebuild the image used by docker
    docker-compose -p "${projectName}" -f ./docker/docker-compose.yml build ;;
  start)
    #h# Start the container
    docker-compose -f ./docker/docker-compose.yml -p "${projectName}" start ;;
  stop)
    #h# Stop the container
    docker-compose -f ./docker/docker-compose.yml -p "${projectName}" stop ;;
  down)
    #h# Stop and remove the container
    docker-compose -f ./docker/docker-compose.yml -p "${projectName}" down ;;
  run)
    #o# [bash command]
    #h# Run a bash command in the container
    shift
    $win docker exec -it "${projectName}-angularcli" $* ;;
  connect)
    #h# Connect to the container
    _color 31 "to exit run Ctrl + q Ctrl + q"
    $win docker exec -it "${projectName}-angularcli" /bin/bash ;;
  debug)
    #o# [Docker-compose options or environnement variables]
    #h# Launch the docker stack and print the logs
    shift
    docker-compose -p "${projectName}" -f ./docker/docker-compose.yml up $* ;;
  config)
    #h# Display the docker-compose config (using the .env)
    #o# [-e KEY=VAR]
    shift
    docker-compose -f ./docker/docker-compose.yml $* config ;;
#### ANGULAR AND NPM COMMANDS 95
  init)
    #h# Create an angular project
    #o# [ng new -options]
    shift
    ./run.sh up
    $win docker exec -it "${projectName}-angularcli" ng new "${projectName}" $*
    cp -R {.env,run.sh,docker,doc} "${projectName}/"
    ./run.sh down
    _color 32 "Angular application is created, now you can run [$ cd to ${projectName}] and enjoy it !" ;;
  version|-v|--version)
    #h# Get the version of npm and node
    echo "NPM version: "
    _color 32 "$(docker exec -it "${projectName}-angularcli" npm -v)"
    echo "Node version: "
    _color 32 "$(docker exec -it "${projectName}-angularcli" node -v)"
    echo "Angular versions: $(docker exec -it "${projectName}-angularcli" ng version)";;
  serve)
    #h# Serve the angular project on your localhost
    #o# [ng serve options]
    shift
    $win docker exec -it "${projectName}-angularcli" ng serve --watch --host 0.0.0.0 --public-host "${projectName,,}.localhost" --port "${angularport} $*"
    _green "you can acces the app via http://${projectName}.localhost/index.html " ;;
  ng)
    #h# Run a NG command
    #o# [ng command and options]
    shift
    $win docker exec -it "${projectName}-angularcli" ng $* ;;
  npm)
    #h# Run a NPM command
    #o# [npm command and options]
    shift
    $win docker exec -it "${projectName}-angularcli" npm $* ;;
#### Webserveurs commands 34
  nginx)
    #h# Build and deploy the project in an nginx container
    #o# [--prod]
    #hh#
    shift
    ./run.sh ng build $*
    docker-compose -f docker/nginx.yml -p "${projectName}-nginx" up -d
    echo "you can acces the app via http://${projectName}-nginx.localhost/index.html "
    echo "if you don't use traefik, acces app via http://localhost:${nginxLocalPort}/index.html"
    ;;
  nginx-stop)
    #h# Stop the nginx web server
    docker-compose -f docker/nginx.yml -p "${projectName}-nginx" down ;;
  nginx-connect)
    #h# Connect to the nginx container if started
    _color 31 "to exit run Ctrl + q Ctrl + q"
    $win docker exec -it "${projectName}-nginx" /bin/bash ;;
  httpd)
    #h# Build and deploy the project in an httpd ontainer
    #o# [--prod]
    #hh#
    shift
    ./run.sh ng build $*
    docker-compose -f docker/httpd.yml -p "${projectName}-httpd" up -d
    echo "you can acces the app via http://${projectName}-httpd.localhost/index.html "
#    echo "if you don't use traefik, acces app via http://localhost:${httpdLocalPort}/index.html"
    ;;
  httpd-stop)
    #h# Stop the httpd web server
    docker-compose -f docker/httpd.yml -p "${projectName}-httpd" down ;;
  httpd-connect)
    #h# Connect to the httpd container if started
    _color 31 "to exit run Ctrl + q Ctrl + q"
    $win docker exec -it "${projectName}-httpd" /bin/bash ;;
#### HELP 31
  --help|-h|help|usage)
    #h# display help
    usage ;;
  *) _color 31 "The super ./run.sh cannot understand what you said, sorry :( "
    echo "see './run.sh help' "
esac

#TODO modify connect to accept connection if containers are alpine based
