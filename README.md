# Angular development environment using Docker :whale: 
This project allows you to create, develop and test angular project in every angular version, without installing Node, npm or angularCli.

All you need is [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/) :hearts: 
## Basic Usage
### Initiate an angular project
* you can change the node / npm version by changing the var **NodeTag** in .env with another tag. Choose this tag according to your needs on [dockerhub](https://hub.docker.com/_/node/?tab=tags) 
* choose your angular cli version and write it in .env, the corresponding var is **angularCliV**
* write your project name in .env (projectName)
* now you can run `./run.sh init [ng cli options for create app]`
* after creating the app, cd to the newly created app directory
* you can init a git repository or move the project's directory
* run `./run.sh build && ./run.sh up`
* you can now use every commands described in command section 

### Use it in a existing angular project
* copy the `docker` directory and the `.env` and `run.sh` file in your app directory
* cd to your app directory
* adapt the versions according to yours needs in `.env` and `docker/Dockerfile` 
* run `./run.sh build && ./run.sh up`
* you can now use every commands described in  commands section.

## Commands
run `./run.sh usage` to display all available commands.  

The following pictures show the output of `./run.sh usage`

![Alt text](doc/usage.png?raw=true "Title")

## Troubleshooting
If a command returns something like   
```
Error: No such container: app-angularcli  
or Error response from daemon: Container XXX is not running 
```
Ensure the container is started with `./run.sh up` 

### Windows Usage:

#### Use Gitbash:
If you use gitbash, the -t option (tty) will not works. An error like `The input device is not a TTY` will be displayed. <br>
You can remove the ``-t`` option but this option is needed to cancel running command in terminal (for exemple to cancel 
``ng serve``) when you tape `Ctrl + c`. <br>

Workaround: A simple workaround is to use `winpty` for all commands including `-t`. This option is handle in run.sh script. <br>
**To enable it, just uncomment the line containing ``#win=winpty`` in the `.env` file.**

#### Use PowerShell:
PowerShell accepts the option -t for docker commands. But in basic configuration, can't run bash script.
To run shell script on powershell, you'll need to enable linux on Windows. See the following 
[instructions](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/). <br>
Note: You just need to do this part:  ``How to Install Bash on Windows 10``

After that, you'll be able to run `run.sh` script by using `bash run.sh` on PowerShell, enjoy ;).


## Use traefik and rebuild the docker image:
### Access ng app through traefik
Basically, this project is configured to use traefik as reverse proxy/ 
Please launch and configure it with [docker-utils](https://gitlab.com/emile_ch/docker-utils/)  
*NB:* Ensure the networkFrontend values in .env are the sames in the 2 projects.

If you don't want to use traefik, uncomment *ports* lines in docker/docker-compose.yml.
After run `./run.sh run serve` you can access app by clicking the link provided in your term as shown here:  
`** Angular Live Development Server is listening on 0.0.0.0:4200, open your browser on http://localhost:4200/ **
`

### Rebuild the docker image after changes in versions
You will need to rebuild the image if you change one if the followings vars in .env: 
* NodeTag
* angularCliV
* typescriptV
* Or if you modify the Dockerfile
  
In order to rebuild the image, run `/run.sh build`


### Useful aliases:
Copy paste the following lines in your *bashrc* file or *aliases* file:
```
alias run="./run.sh"
alias ng="run ng"
alias npm="run npm"
```
You can now use `run` instead of `./run.sh`, `ng` instead of `.run.sh ng` and `npm` instead of `.run.sh npm`.  
Of course, you can replace `run` `npm` `ng` by a preferred alias name.

